from ffmpy import FFprobe
import json
import subprocess


"""This module will collect all the meta info from a video file and will return it in json format.
            ffprobe -i input.mp4 -hide_banner -loglevel fatal
                    -show_error -show_format -show_streams
                    -show_programs -show_chapters
                    -show_private_data -print_format json"""

_remove_from_video = ['index', 'codec_long_name', 'codec_time_base', 'codec_tag_string', 'codec_tag',
                      'has_b_frames', 'color_space', 'color_transfer', 'color_primaries', 'chroma_location',
                      'refs', 'is_avc', 'nal_length_size', 'r_frame_rate', 'time_base', 'start_pts',
                      'start_time', 'duration_ts', 'nb_frames', 'disposition', 'tags', 'color_range']
_remove_from_audio = ['index', 'codec_long_name', 'codec_time_base', 'codec_tag_string', 'codec_tag',
                      'sample_fmt', 'channel_layout', 'bits_per_sample', 'r_frame_rate', 'avg_frame_rate',
                      'time_base', 'start_pts', 'start_time', 'duration_ts', 'disposition', 'tags']
_remove_from_format = ['nb_programs', 'format_long_name', 'probe_score', 'tags']


ffprobe = FFprobe(
    inputs={'input.mp4': '-hide_banner -loglevel fatal -show_streams -show_format -print_format json'})
proc = subprocess.Popen(ffprobe.cmd, stdout=subprocess.PIPE)
raw_output = proc.stdout.read()

raw_json_output = json.loads(raw_output)
streams = raw_json_output['streams']


def get_stream(st, av):
    """Extracts the stream information from a dict of streams and returns another dict per stream type"""
    av_s = {}
    for item in range(len(st)):
        if st[item]['codec_type'] == av:
            av_s = st[item]
    return av_s


video_stream = get_stream(streams, 'video')
audio_stream = get_stream(streams, 'audio')
file_meta = raw_json_output['format']

for i in _remove_from_video:
    del video_stream[i]

for i in _remove_from_audio:
    del audio_stream[i]

for i in _remove_from_format:
    del file_meta[i]


# (lambda x: x * 3)(5)
# print(dict(filter(function, a_list)))

# print(json.dumps(file_meta, indent=2))
print(json.dumps(video_stream, indent=2))
print(json.dumps(audio_stream, indent=2))
